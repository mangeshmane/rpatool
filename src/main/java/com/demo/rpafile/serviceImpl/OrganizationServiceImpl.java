package com.demo.rpafile.serviceImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.demo.rpafile.controller.OrganizationController;
import com.demo.rpafile.dao.Contact_PersonRepo;
import com.demo.rpafile.dao.OrganizationRepo;
import com.demo.rpafile.model.Contact_Person;
import com.demo.rpafile.model.Organization;
import com.demo.rpafile.model.Result;
import com.demo.rpafile.service.OrganizationService;

@Service("OrganizationService")
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	OrganizationRepo organizationRepo;

	@Autowired
	Contact_PersonRepo cPersonRepo;

	private static final Logger logger = LoggerFactory.getLogger(OrganizationServiceImpl.class);
	
	@Override
	public ResponseEntity<Result<Organization>> saveOrganization(Organization org) {
		Result<Organization> result = new Result<Organization>();
		Organization o = organizationRepo.save(org);
		//Contact_Person cPerson = cPersonRepo.findByNameAndemail_id(org.getContact_person().getName(),org.getContact_person().getEmail_id());
		try {
			if (o != null) {
			 	//Organization o = organizationRepo.save(org);
			 	result.setObj(o);
				result.setSuccess(Boolean.TRUE);
				result.setSuccessMessage("Organization Saved Successfully");
				result.setError("none");
			} else {
				result.setSuccess(Boolean.FALSE);
				result.setError("Contact person not found");
			}
		} catch (Exception e) {
			result.setSuccess(Boolean.FALSE);
			result.setError("error while saving data is: " + e.getMessage()+".");
			logger.error("error while saving data: " + e);
		}

		return ResponseEntity.ok(result);
	}

	@Override
	public ResponseEntity<Result> updateOrganization(Integer id, Organization org) {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		Organization or = organizationRepo.findById(id).get();
		Result result = new Result();
		if ((or != null)) {
			or.setIndustry(org.getIndustry());
			or.setServices(org.getServices());
			or.setVolume_of_operations(org.getVolume_of_operations());
			or.setLatitude(org.getLatitude());
			or.setLongitude(org.getLongitude());
			or.setAddress(org.getAddress());
			organizationRepo.save(or);
			result.setSuccess(Boolean.TRUE);
			result.setError("no error occured");
			result.setSuccessMessage("Organization UPDATED SUCCEESSFULLY");

		} else {
			result.setSuccess(Boolean.FALSE);
			result.setError("error occured");
			result.setSuccessMessage("Organization NOT UPDATED");

		}
		return ResponseEntity.ok(result);
	}

	@Override
	public ResponseEntity<List<Organization>> getAllOrganization() {
		// String name =
		// SecurityContextHolder.getContext().getAuthentication().getName();
		List<Organization> list = organizationRepo.findAll();
		return ResponseEntity.ok(list);
	}
}
