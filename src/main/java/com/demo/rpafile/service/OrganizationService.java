package com.demo.rpafile.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.demo.rpafile.model.Organization;
import com.demo.rpafile.model.Result;

public interface OrganizationService {

	public ResponseEntity<Result<Organization>> saveOrganization(Organization org);
	
	public  ResponseEntity<Result> updateOrganization(Integer id, Organization org);
	
	public ResponseEntity<List<Organization>> getAllOrganization();
}
