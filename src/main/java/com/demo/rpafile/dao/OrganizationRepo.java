package com.demo.rpafile.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.rpafile.model.Organization;

@Repository
@Transactional
public interface OrganizationRepo extends JpaRepository<Organization, Integer> {

}
