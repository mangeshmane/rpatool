package com.demo.rpafile.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.rpafile.model.Contact_Person;

@Repository 
@Transactional
public interface Contact_PersonRepo extends JpaRepository<Contact_Person, Integer>{
	
	@Query("Select c.name, c.email_id from Contact_Person c where c.name = :name and c.email_id = :email")
	Contact_Person findByNameAndemail_id(@Param("name") String c_name, @Param("email")String c_email_id);

}
