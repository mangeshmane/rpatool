package com.demo.rpafile.controller;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rpafile.model.Organization;
import com.demo.rpafile.model.Result;
import com.demo.rpafile.service.OrganizationService;

@RestController
@RequestMapping(path = "/organization")
public class OrganizationController {
	
	private static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);
	@Autowired
	OrganizationService organizationService; 
	
	@PreAuthorize("hasAuthority ('USER')")
	@PostMapping(path = "/save")
	public ResponseEntity<Result<Organization>> saveOrganization(@RequestBody Organization org ){
		try {
			return organizationService.saveOrganization(org);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ResponseEntity.ok(new Result());
		
	}
	@PreAuthorize("hasAuthority ('USER')")
	@PutMapping(path = "/updateOrg/{id}")
	public ResponseEntity<Result> updateOrganization(@PathVariable("id") Integer id, @RequestBody Organization org ){
		try {
			return organizationService.updateOrganization(id , org);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ResponseEntity.ok(new Result());
		
	}
	//@PreAuthorize("hasAuthority ('USER')")
	@GetMapping(path = "/getAllOrganization")
	public ResponseEntity<List<Organization>> getAllOrganization(){
		try {
			return organizationService.getAllOrganization();
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return ResponseEntity.ok(new ArrayList<>());
		
	}
}
